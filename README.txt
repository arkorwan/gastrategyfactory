=======================================================================
 Genetic Algorithm for Solving Equations with a Single Integer Vaiable
=======================================================================

COMPILATION
===========

To compile all sources from the root directory:

javac -d ./bin -cp ./lib/asg.cliche-110413.jar:./lib/expr.jar  ./src/main/java/me/arkorwan/cs613/ga/*.java ./src/main/java/me/arkorwan/cs613/ga/*/*.java ./src/main/java/me/arkorwan/utils/*.java


RUN
===

To run:

java -cp ./lib/asg.cliche-110413.jar:./lib/expr.jar:./bin me.arkorwan.cs613.ga.GARunner

This will open a GAStrategyFactory shell. Type ?l to see the list of available commands.

> ?l

abbrev	name	params
s	solve	(p1)
sss	set-selection-strategy	(p1)
scs	set-crossover-strategy	(p1)
sms	set-mutation-strategy	(p1)
pgp	print-GA-parameters	()
smp	set-mutation-probability	(p1)
scp	set-crossover-probability	(p1)
stps	set-target-population-size	(p1)
sil	set-iteration-limit	(p1)

Type pg. to see the current configuration.

> pgp

  Selection strategy = null
  Crossover strategy = null
  Mutation strategy = null
  Population size = 0.8
  Crossover probability = 0.05
  Algorithm iteration limit = 10000

The strategies are not set yet. You have to set all of them before try solving equations. List of all valid strategies is given in the notes.pdf file. For now, let's setup some easy-to-type strategies and print the parameters out.

> sss rank
> scs singlepoint
> sms bitflip:0.2
> pgp

  Selection strategy = rank selection strategy
  Crossover strategy = single-point crossover strategy
  Mutation strategy = Bit flipping mutation strategy with expected flip bits = 0.20%
  Population size = 0.8
  Crossover probability = 0.05
  Algorithm iteration limit = 10000

Now, to solve equations, just type: solve [equation]. The equation needs exactly one variable, and that variable must be "x". And no space is allowed. For example, to solve (x*x)+123 = x*456+789, type:

> solve (x*x)+123=x*456+789

This is the result I got:

Rearrange expression => 0 = ((x*x)+123)-(x*456+789)
Start running genetic algorithm
  Selection strategy = rank selection strategy
  Crossover strategy = single-point crossover strategy
  Mutation strategy = Bit flipping mutation strategy with expected flip bits = 0.20%
  Population size = 0.8
  Crossover probability = 0.05
  Algorithm iteration limit = 10000
Terminate on iteration 10000
best answer: 1358990828
 with fitness: -2147483647

That’s one bad answer. To improve, try configure the parameters a bit. Let’s increase the iteration limit from 10000 to 100000

> sil 100000
> solve (x*x)+123=x*456+789

Start running genetic algorithm
  Selection strategy = rank selection strategy
  Crossover strategy = single-point crossover strategy
  Mutation strategy = Bit flipping mutation strategy with expected flip bits = 0.20%
  Population size = 0.8
  Crossover probability = 0.05
  Algorithm iteration limit = 100000
Terminate on iteration 100000
best answer: 457
 with fitness: -209

That’s better. To quit from the shell, type exit.

> exit

See notes.pdf for some more notes on the usage and implementation detail of this project.