package me.arkorwan.cs613.ga.operator;

import static org.testng.Assert.assertEquals;

import java.util.Arrays;
import java.util.List;

import me.arkorwan.cs613.ga.operator.RankSelectionStrategy;

import org.testng.annotations.Test;

public class TestRankSelectionStrategy{
	
	@Test
	public void testGetWeights(){
		List<Integer> w = Arrays.asList(5,1,0,-4,10);
		assertEquals(new RankSelectionStrategy().getWeights(w), new int[]{4, 3, 2, 1, 5});
	}

}
