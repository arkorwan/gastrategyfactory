package me.arkorwan.cs613.ga.operator;

import static org.testng.Assert.assertEquals;
import me.arkorwan.cs613.ga.operator.AbstractRouletteSelectionStrategy;

import org.testng.annotations.Test;

public class TestAbstractRouletteSelectionStrategy {
	
	@Test
	public void testNormalizeWeights(){
		int[] w = {1,2,3,4,5};
		AbstractRouletteSelectionStrategy.normalizeWeights(w);
		assertEquals(w, new int[] {0, 2, 5, 9, 14});
	}
	
	@Test
	public void testGetWeightIndexFromValue(){
		assertEquals(AbstractRouletteSelectionStrategy.getWeightIndexFromValue(8, new int[] {2, 5, 10, 20}), 2);
	}
}
