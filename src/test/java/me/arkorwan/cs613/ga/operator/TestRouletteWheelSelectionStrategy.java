package me.arkorwan.cs613.ga.operator;

import static org.testng.Assert.*;

import java.util.Arrays;
import java.util.List;

import me.arkorwan.cs613.ga.operator.RouletteWheelSelectionStrategy;

import org.testng.annotations.Test;

public class TestRouletteWheelSelectionStrategy {
	
	@Test
	public void testGetWeights(){
		RouletteWheelSelectionStrategy strategy = new RouletteWheelSelectionStrategy(0.1);
		List<Integer> fitness = Arrays.asList(-4,1,0,-8,10);
		int[] actual = strategy.getWeights(fitness);
		assertEquals(actual, new int[] {300, 550, 500, 100, 1000});
	}
	
}
