package me.arkorwan.utils;

public class IntegerBitConverter {

	//convert int to boolean[]
	public static boolean[] bitArrayFromInt(int value){
		boolean[] b = new boolean[Integer.SIZE];
		int v = value;
		for(int i = 0; i<b.length; i++){
			b[i] = (v&1) == 1;
			v >>= 1;
		}
		return b;
	}
	
	//convert boolean[] to int
	public static int intFromBitArray(boolean[] b){
		int value = 0;
		int v = 1;
		for(int i = 0; i<b.length; i++){
			if(b[i]){
				value += v;
			}
			v <<= 1;
		}
		return value;
	}

}
