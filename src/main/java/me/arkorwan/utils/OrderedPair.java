package me.arkorwan.utils;

// an OrderedPair is a Pair where both objects are Comparable. Comparing two
// OrderedPairs is to compare the first member, then if equal compare the
// second member.
public class OrderedPair<T extends Comparable<T>, U extends Comparable<U>>
		extends Pair<T, U> implements Comparable<OrderedPair<T, U>> {

	public OrderedPair(T x, U y) {
		super(x, y);
	}

	@Override
	public int compareTo(OrderedPair<T, U> o) {
		// TODO: guard against NULL!
		int v = this.first.compareTo(o.first);
		if (v == 0) {
			return this.second.compareTo(o.second);
		} else {
			return v;
		}
	}

}
