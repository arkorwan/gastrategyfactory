package me.arkorwan.utils;

// A simple immutable pair of objects.
public class Pair<T, U> {

	public final T first;
	public final U second;

	public Pair(T x, U y) {
		first = x;
		second = y;
	}

}
