package me.arkorwan.utils;

// A Function (a:boolean, b:boolean):boolean. Subclasses just need to override to process(a, b) method.

public abstract class BitBinaryFunction {

	public abstract boolean process(boolean a, boolean b);

	// Some common functions provided as static final members:
	// SELECT_FIRST(a, b) => a
	// SELECT_SECOND(a, b) => b
	// AND(a,b) => a&b
	// OR(a,b) => a|b
	// XOR(a,b) => a xor b
	public static class Functions {

		public static final BitBinaryFunction SELECT_FIRST = new BitBinaryFunction() {

			@Override
			public boolean process(boolean a, boolean b) {
				return a;
			}

		};

		public static final BitBinaryFunction SELECT_SECOND = new BitBinaryFunction() {

			@Override
			public boolean process(boolean a, boolean b) {
				return b;
			}

		};

		public static final BitBinaryFunction AND = new BitBinaryFunction() {

			@Override
			public boolean process(boolean a, boolean b) {
				return a & b;
			}

		};

		public static final BitBinaryFunction OR = new BitBinaryFunction() {

			@Override
			public boolean process(boolean a, boolean b) {
				return a | b;
			}

		};

		public static final BitBinaryFunction XOR = new BitBinaryFunction() {

			@Override
			public boolean process(boolean a, boolean b) {
				return a ^ b;
			}

		};
	}

}
