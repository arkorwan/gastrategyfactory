package me.arkorwan.cs613.ga;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

import me.arkorwan.cs613.ga.operator.CrossoverStrategy;
import me.arkorwan.cs613.ga.operator.MutationStrategy;
import me.arkorwan.cs613.ga.operator.SelectionResult;
import me.arkorwan.cs613.ga.operator.SelectionStrategy;
import me.arkorwan.utils.Pair;
import asg.cliche.Command;

// The main genetic algorithm framework
public class GeneticAlgorithmExecutor<T extends Evolvable> {

	private EvolvableFactory<T> factory;
	private FitnessEvaluator<T> evaluator;
	private SelectionStrategy selector;
	private CrossoverStrategy crosser;
	private MutationStrategy mutator;
	private int targetPopulationSize;
	private double mutationProbability;
	private double crossoverProbability;

	private int iterationLimit;

	public GeneticAlgorithmExecutor(EvolvableFactory<T> factory) {
		this.factory = factory;

		// default values
		this.setTargetPopulationSize(30);
		this.setMutationProbability(0.05);
		this.setCrossoverProbability(0.8);
		this.setIterationLimit(10000);
	}

	public T run() {
		System.out.println("Start running genetic algorithm");
		printGAParameters();

		List<T> population = null;
		int i = 0;
		T fittest;
		int bestFitness;

		// loop until exceeds ith generation, or perfect solution is found
		do {
			population = produceNewGeneration(population);
			fittest = getFittestMember(population);
			bestFitness = getEvaluator().getFitness(fittest);
		} while (++i < getIterationLimit()
				&& bestFitness != getEvaluator().bestFitness());
		System.out.println("Terminate on iteration " + i);

		// return best solution from the last generation
		return fittest;
	}

	@Command
	public void printGAParameters() {
		System.out.println("  Selection strategy = " + getSelector());
		System.out.println("  Crossover strategy = " + getCrosser());
		System.out.println("  Mutation strategy = " + getMutator());
		System.out.println("  Population size = " + getCrossoverProbability());
		System.out.println("  Crossover probability = "
				+ getMutationProbability());
		System.out.println("  Algorithm iteration limit = "
				+ getIterationLimit());
	}

	// create random population
	private List<T> produceInitialGeneration() {
		List<T> population = new ArrayList<T>();
		for (int i = 0; i < this.targetPopulationSize; i++) {
			population.add(factory.createRandomInstance());
		}
		return population;
	}

	// create new generation from previous generation
	private List<T> produceNewGeneration(List<T> population) {
		if (population == null || population.size() == 0) {
			return produceInitialGeneration();
		}

		List<T> newGeneration = new ArrayList<T>();
		// evaluate fitness for each Chromosome as a separate list with the same
		// order
		List<Integer> fitness = new ArrayList<Integer>();
		for (T p : population) {
			fitness.add(getEvaluator().getFitness(p));
		}

		// Create a SelectionResult iterator
		Iterator<SelectionResult> indicesIterator = getSelector()
				.getSelectionIterator(fitness);
		assert indicesIterator.hasNext(); // shouldn't be empty

		T potentialParent = null;
		// loop until the population has enough members
		while (newGeneration.size() < targetPopulationSize) {

			// recreate the selector if necessary
			if (!indicesIterator.hasNext()) {
				indicesIterator = getSelector().getSelectionIterator(fitness);
			}

			SelectionResult result = indicesIterator.next();
			T member = population.get(result.index);
			if (!result.isRecombinationAllowed) {
				// no crossover/mutation allowed, add directly to new generation
				addMemberOfNewGeneration(member, newGeneration, false);
			} else if (ThreadLocalRandom.current().nextDouble() < getCrossoverProbability()) {
				if (potentialParent == null) {
					// keep this as a crossover parent
					potentialParent = member;
				} else {
					// found a couple! do the crossover and add the offsprings
					addCrossoverOffspringsToNewGeneration(potentialParent, member, newGeneration);
					// clear the first parent
					potentialParent = null;
				}
			} else {
				// no crossover but mutation could happen
				addMemberOfNewGeneration(member, newGeneration, true);
			}
		}
		return newGeneration;
	}

	private void addCrossoverOffspringsToNewGeneration(T parent1, T parent2, List<T> memberList){
		Pair<T, T> offsprings = getCrosser().execute(factory,
				parent1, parent2);
		addMemberOfNewGeneration(offsprings.first, memberList,
				true);
		addMemberOfNewGeneration(offsprings.second, memberList,
				true);
	}
	
	// add new member with a chance of mutation if allowed
	private void addMemberOfNewGeneration(T member, List<T> memberList,
			boolean mutationAllowed) {
		if (mutationAllowed
				&& ThreadLocalRandom.current().nextDouble() < getMutationProbability()) {
			member = getMutator().execute(factory, member);
		}
		memberList.add(member);
	}

	private T getFittestMember(List<T> population) {
		T maxMember = population.get(0);
		int maxFitness = getEvaluator().getFitness(maxMember);

		for (int i = 1; i < population.size(); i++) {
			T member = population.get(i);
			int fitness = getEvaluator().getFitness(member);
			if (fitness > maxFitness) {
				maxFitness = fitness;
				maxMember = member;
			}
		}

		return maxMember;
	}

	// getter-setter methods

	public double getMutationProbability() {
		return mutationProbability;
	}

	@Command
	public void setMutationProbability(double mutationProbability) {
		this.mutationProbability = mutationProbability;
	}

	public double getCrossoverProbability() {
		return crossoverProbability;
	}

	@Command
	public void setCrossoverProbability(double crossoverProbability) {
		this.crossoverProbability = crossoverProbability;
	}

	public int getTargetPopulationSize() {
		return targetPopulationSize;
	}

	@Command
	public void setTargetPopulationSize(int targetPopulationSize) {
		this.targetPopulationSize = targetPopulationSize;
	}

	public int getIterationLimit() {
		return iterationLimit;
	}

	@Command
	public void setIterationLimit(int iterationLimit) {
		this.iterationLimit = iterationLimit;
	}

	public SelectionStrategy getSelector() {
		return selector;
	}

	public void setSelector(SelectionStrategy selector) {
		this.selector = selector;
	}

	public CrossoverStrategy getCrosser() {
		return crosser;
	}

	public void setCrosser(CrossoverStrategy crosser) {
		this.crosser = crosser;
	}

	public MutationStrategy getMutator() {
		return mutator;
	}

	public void setMutator(MutationStrategy mutator) {
		this.mutator = mutator;
	}

	public FitnessEvaluator<T> getEvaluator() {
		return evaluator;
	}

	public void setEvaluator(FitnessEvaluator<T> evaluator) {
		this.evaluator = evaluator;
	}
}
