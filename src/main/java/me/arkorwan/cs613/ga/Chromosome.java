package me.arkorwan.cs613.ga;

import me.arkorwan.utils.BitBinaryFunction;
import me.arkorwan.utils.Pair;

// Genetic chromosome encoded as an array of booleans.
public class Chromosome implements Comparable<Chromosome> {

	private boolean[] rawBits;

	private Chromosome() {
	};

	public static Chromosome fromBitSet(boolean[] raw) {
		Chromosome chr = new Chromosome();
		chr.rawBits = (boolean[]) raw.clone();
		return chr;
	}

	@Override
	public boolean equals(Object other) {
		if (other == null || !(other instanceof Chromosome)) {
			return false;
		} else {
			Chromosome otherC = (Chromosome) other;
			if (this.rawBits.length != otherC.rawBits.length) {
				return false;
			} else {
				for (int i = 0; i < this.rawBits.length; i++) {
					if (this.rawBits[i] != otherC.rawBits[i])
						return false;
				}
				return true;
			}
		}
	}

	// Natural ordering of Chromosomes: 1) Shorter chromosomes come before. 2)
	// For Chromosomes with the same length, compare each bit from left to
	// right. False comes before True.
	@Override
	public int compareTo(Chromosome o) {
		// number of bits
		int lengthDiff = this.rawBits.length - o.rawBits.length;
		if (lengthDiff != 0) {
			return lengthDiff;
		} else {
			// compare each bit; leftmost bit is the LSB
			for (int i = 0; i < this.rawBits.length; i++) {
				if (this.rawBits[i] && !o.rawBits[i]) {
					return 1;
				} else if (!this.rawBits[i] && o.rawBits[i]) {
					return -1;
				}
			}
			return 0;
		}
	}

	public int length() {
		return rawBits.length;
	}

	// return a copy of underlying array.
	public boolean[] getBits() {
		return rawBits.clone();
	}

	public boolean getBit(int index) {
		return rawBits[index];
	}

	public void setBit(int index, boolean value) {
		rawBits[index] = value;
	}

	public void flipBit(int index) {
		rawBits[index] = !rawBits[index];
	}

	// Create new Chromosome from this one by concatenating with the given
	// Chromosome.
	public Chromosome append(Chromosome other) {
		boolean[] b = new boolean[this.rawBits.length + other.rawBits.length];
		System.arraycopy(this.rawBits, 0, b, 0, this.rawBits.length);
		System.arraycopy(other.rawBits, 0, b, this.rawBits.length,
				other.rawBits.length);
		return Chromosome.fromBitSet(b);
	}

	// Split a Chromosome into two, with the first having the specify length.
	// Behaviour is unknown when the length given is larger than this
	// Chromosome.
	public Pair<Chromosome, Chromosome> split(int firstLength) {
		boolean[] b1 = new boolean[firstLength];
		boolean[] b2 = new boolean[this.rawBits.length - firstLength];
		System.arraycopy(this.rawBits, 0, b1, 0, firstLength);
		System.arraycopy(this.rawBits, firstLength, b2, 0, this.rawBits.length
				- firstLength);
		return new Pair<Chromosome, Chromosome>(Chromosome.fromBitSet(b1),
				Chromosome.fromBitSet(b2));
	}

	// Create new Chromosome with the same bits as this one but in reverse.
	public Chromosome reverse() {
		boolean[] b = new boolean[this.rawBits.length];
		for (int i = 0; i < b.length; i++)
			b[i] = rawBits[b.length - 1 - i];
		return Chromosome.fromBitSet(b);
	}

	// Create new Chromosome with the same bits as this one, but reverse the
	// ordering of bits in the range [from, from+length).
	public Chromosome reverse(int from, int length) {
		boolean[] b = new boolean[this.rawBits.length];
		System.arraycopy(this.rawBits, 0, b, 0, b.length);
		for (int i = from; i < from + length; i++)
			b[i] = rawBits[from + length - 1 - i];
		return Chromosome.fromBitSet(b);
	}

	// Extract bits in range [from, from+length) out as a new Chromosome.
	public Chromosome range(int from, int length) {
		if (from < 0 || from + length > this.rawBits.length) {
			throw new IllegalArgumentException("range out of bound");
		}
		boolean[] b = new boolean[length];
		System.arraycopy(this.rawBits, from, b, 0, length);
		return Chromosome.fromBitSet(b);
	}

	// Create new Chromosome by applying the given operation on each bit of this
	// Chromosome and the given one.
	public Chromosome applyBinary(BitBinaryFunction func, Chromosome other) {
		if (this.length() != other.length()) {
			throw new IllegalArgumentException(
					"chromosomes have different bit length");
		}
		boolean[] b = new boolean[this.rawBits.length];
		for (int i = 0; i < b.length; i++) {
			b[i] = func.process(this.rawBits[i], other.rawBits[i]);
		}
		return Chromosome.fromBitSet(b);
	}

}
