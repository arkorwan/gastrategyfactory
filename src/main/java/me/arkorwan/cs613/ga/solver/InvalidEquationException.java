package me.arkorwan.cs613.ga.solver;

public class InvalidEquationException extends Exception{

	private static final long serialVersionUID = -5248214139994110424L;

	public InvalidEquationException(String message){
		super(message);
	}
	
	public InvalidEquationException(String message, Exception innerException){
		super(message, innerException);
	}
	
}
