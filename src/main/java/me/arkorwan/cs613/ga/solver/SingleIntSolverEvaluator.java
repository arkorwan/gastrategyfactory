package me.arkorwan.cs613.ga.solver;

import me.arkorwan.cs613.ga.FitnessEvaluator;
import expr.Expr;
import expr.Parser;
import expr.SyntaxException;
import expr.Variable;

public class SingleIntSolverEvaluator implements
		FitnessEvaluator<SingleIntSolver> {

	private Expr expr;
	private Variable x = Variable.make("x");

	// create an evaluator from equation string. Equation must contains a single
	// equal sign ("="). Equation A(x)=B(x) is rearranged into A(x)-B(x)=0, then
	// the value of A-B is evaluated to see how close the result is to 0.
	public SingleIntSolverEvaluator(String equationString)
			throws InvalidEquationException {
		String expression = getExpressionFromEquation(equationString); // might
																		// throw
																		// exception
		validateExpression(expression); // might throw exception
		System.out.println("Rearrange expression => 0 = " + expression);
	}

	private String getExpressionFromEquation(String equationString)
			throws InvalidEquationException {
		String[] sides = equationString.split("=");
		if (sides.length != 2) {
			throw new InvalidEquationException(
					"Invalid equation - need exactly 1 equal sign (\"=\")");
		}
		// rearrange a=b as a-b=0, then we can focus on a-b
		return String.format("(%s)-(%s)", sides[0], sides[1]);
	}

	// try parsing the expression
	private void validateExpression(String expression)
			throws InvalidEquationException {
		Parser parser = new Parser();
		parser.allow(x);
		try {
			expr = parser.parseString(expression);
		} catch (SyntaxException e) {
			System.out.println(e.explain());
			throw new InvalidEquationException(
					"Invalid equation - fail parsing expression", e);
		}
	}

	// fitness is -ABS(f(x)). So that the best solution (f(x)=0) has maximum
	// possible value of 0.
	@Override
	public int getFitness(SingleIntSolver object) {
		x.setValue(object.getValue());
		int v = (int) Math.abs(expr.value());
		return -v;
	}

	// best fitness is when f(x) = 0
	@Override
	public int bestFitness() {
		return 0;
	}

}
