package me.arkorwan.cs613.ga.solver;

import java.util.concurrent.ThreadLocalRandom;

import me.arkorwan.cs613.ga.Chromosome;
import me.arkorwan.cs613.ga.Evolvable;
import me.arkorwan.cs613.ga.EvolvableFactory;
import me.arkorwan.utils.IntegerBitConverter;

// The problem representation. Solution is a single int.
// Encoding is simply a representation of the int in binary.
public class SingleIntSolver implements Evolvable {

	// singleton anonymous factory instance
	private static EvolvableFactory<SingleIntSolver> factoryInstance = new EvolvableFactory<SingleIntSolver>() {

		// convert the chromosome to an int, then create new instance
		@Override
		public SingleIntSolver createInstanceFromChromosome(Chromosome chr) {
			return new SingleIntSolver(chr);
		}

		// random instance is just a new instance from a random number
		@Override
		public SingleIntSolver createRandomInstance() {
			return new SingleIntSolver(ThreadLocalRandom.current().nextInt());
		}
	};

	private Chromosome chromosome;
	private int value;

	private SingleIntSolver(int d) {
		this.value = d;
		this.chromosome = Chromosome.fromBitSet(IntegerBitConverter
				.bitArrayFromInt(value));

	}

	private SingleIntSolver(Chromosome chr) {
		this.chromosome = chr;
		this.value = IntegerBitConverter.intFromBitArray(chr.getBits());
	}

	public int getValue() {
		return this.value;
	}

	@Override
	public String toString() {
		return "[x=" + value + "\t]";

	}

	@Override
	public Chromosome getChromosome() {
		return this.chromosome;
	}

	public static EvolvableFactory<SingleIntSolver> getFactory() {
		return factoryInstance;
	}
}
