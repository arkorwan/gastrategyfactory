package me.arkorwan.cs613.ga;

import java.io.IOException;

import asg.cliche.Command;
import asg.cliche.ShellFactory;
import me.arkorwan.cs613.ga.operator.CrossoverStrategy;
import me.arkorwan.cs613.ga.operator.GeneticOperatorException;
import me.arkorwan.cs613.ga.operator.GeneticOperatorStrategyFactory;
import me.arkorwan.cs613.ga.operator.MutationStrategy;
import me.arkorwan.cs613.ga.operator.SelectionStrategy;
import me.arkorwan.cs613.ga.solver.InvalidEquationException;
import me.arkorwan.cs613.ga.solver.SingleIntSolver;
import me.arkorwan.cs613.ga.solver.SingleIntSolverEvaluator;

//The main class.
public class GARunner {

	private GeneticAlgorithmExecutor<SingleIntSolver> executor;

	private GARunner() {
		executor = new GeneticAlgorithmExecutor<SingleIntSolver>(
				SingleIntSolver.getFactory());
	}

	// helper method to validate strategy input
	private static boolean validateNotNull(Object object,
			String objectDescription) {
		if (object == null) {
			System.out.println("Value cannot be null - " + objectDescription);
			return false;
		}
		return true;
	}

	// Execute the algorithm!
	@Command
	public void solve(String equation) {

		// validate the strategies - there's no default strategy given
		if (validateNotNull(executor.getSelector(), "Selection Strategy")
				&& validateNotNull(executor.getCrosser(), "Crossover Strategy")
				&& validateNotNull(executor.getMutator(), "Mutation Strategy")) {
			FitnessEvaluator<SingleIntSolver> evaluator;
			try {
				evaluator = new SingleIntSolverEvaluator(equation);
			} catch (InvalidEquationException e) {
				System.out.println(e.getMessage());
				return;
			}
			executor.setEvaluator(evaluator);

			// execute!
			SingleIntSolver winner = executor.run();

			System.out.println("best answer: " + winner.getValue());
			System.out.println(" with fitness: "
					+ executor.getEvaluator().getFitness(winner));
		}

	}

	@Command
	public void setSelectionStrategy(String descriptor) {
		try {
			SelectionStrategy selector = GeneticOperatorStrategyFactory
					.getSelectionStrategy(descriptor);
			executor.setSelector(selector);
		} catch (GeneticOperatorException e) {
			System.out.println(e.getMessage());
		}
	}

	@Command
	public void setCrossoverStrategy(String descriptor) {
		try {
			CrossoverStrategy crosser = GeneticOperatorStrategyFactory
					.getCrossoverStrategy(descriptor);
			executor.setCrosser(crosser);
		} catch (GeneticOperatorException e) {
			System.out.println(e.getMessage());
		}
	}

	@Command
	public void setMutationStrategy(String descriptor) {
		try {
			MutationStrategy mutator = GeneticOperatorStrategyFactory
					.getMutationStrategy(descriptor);
			executor.setMutator(mutator);
		} catch (GeneticOperatorException e) {
			System.out.println(e.getMessage());
		}
	}

	// Main method. Create the shell.
	public static void main(String[] args) throws IOException {
		GARunner runner = new GARunner();
		ShellFactory.createConsoleShell("GAStrategyFactory", "", runner,
				runner.executor).commandLoop();
	}

}
