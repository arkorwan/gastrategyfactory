package me.arkorwan.cs613.ga;

// Anything with Chromosome is Evolvable
public interface Evolvable {

	Chromosome getChromosome();

}
