package me.arkorwan.cs613.ga.operator;

//Selection result composed of 1) index of the item 2) a flag to indicate
//if the result is allowed to be manipulated further
public class SelectionResult {

	public final int index;
	public final boolean isRecombinationAllowed;

	SelectionResult(int index, boolean isRecombinationAllowed) {
		this.index = index;
		this.isRecombinationAllowed = isRecombinationAllowed;
	}

	SelectionResult(int index) {
		this(index, true);
	}

}
