package me.arkorwan.cs613.ga.operator;

import java.util.concurrent.ThreadLocalRandom;

import me.arkorwan.cs613.ga.Chromosome;
import me.arkorwan.cs613.ga.Evolvable;
import me.arkorwan.cs613.ga.EvolvableFactory;
import me.arkorwan.utils.Pair;

// Uniform crossover strategy. For each bit index, randomly decide to
// 1) swap the value at that index, or
// 2) keep the original values.
class UniformCrossoverStrategy extends CrossoverStrategy {

	@Override
	public <T extends Evolvable> Pair<T, T> execute(
			EvolvableFactory<T> factory, T parent1, T parent2) {

		Chromosome c1 = parent1.getChromosome();
		Chromosome c2 = parent2.getChromosome();

		assert (c1.length() == c2.length());
		int l = c1.length();
		boolean[] cross1 = new boolean[l];
		boolean[] cross2 = new boolean[l];

		for (int i = 0; i < l; i++) {
			if (ThreadLocalRandom.current().nextBoolean()) {
				cross1[i] = c1.getBit(i);
				cross2[i] = c2.getBit(i);
			} else {
				cross1[i] = c2.getBit(i);
				cross2[i] = c1.getBit(i);
			}
		}

		T offspring1 = factory.getNewInstance(Chromosome.fromBitSet(cross1));
		T offspring2 = factory.getNewInstance(Chromosome.fromBitSet(cross2));

		return new Pair<T, T>(offspring1, offspring2);
	}

	@Override
	public String getStrategyDescription() {
		return "Uniform crossover stratgegy";
	}

}
