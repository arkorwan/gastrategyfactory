package me.arkorwan.cs613.ga.operator;

import java.util.concurrent.ThreadLocalRandom;

import me.arkorwan.cs613.ga.Chromosome;
import me.arkorwan.cs613.ga.Evolvable;
import me.arkorwan.cs613.ga.EvolvableFactory;

// Bit reversal mutation strategy. Reverse the order of the bits.
class BitsReverseMutationStrategy extends MutationStrategy {

	@Override
	public <T extends Evolvable> T execute(EvolvableFactory<T> factory, T mutant) {
		Chromosome chr = mutant.getChromosome();
		int x = ThreadLocalRandom.current().nextInt(chr.length());
		int y = ThreadLocalRandom.current().nextInt(chr.length());
		if (x > y) {
			int temp = x;
			x = y;
			y = temp;
		}
		assert (x <= y);
		return factory.getNewInstance(chr.reverse(x, y - x + 1));
	}

	@Override
	public String getStrategyDescription() {
		return "Bits reverse mutation strategy";
	}

}
