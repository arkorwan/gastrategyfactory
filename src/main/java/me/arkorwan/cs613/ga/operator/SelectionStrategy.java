package me.arkorwan.cs613.ga.operator;

import java.util.Iterator;
import java.util.List;

// Parent of all selection strategies
public abstract class SelectionStrategy extends GeneticOperatorStrategy {

	// Perform selection operator on the given fitness of the population.
	// Return a list of SelectionResult, which contains the index of the
	// selected item, and a flag to indicate if the result is allowed for
	// further operations (crossover/mutation).
	// Size of the returned list is expected to be the same as the population
	// size.
	public abstract Iterator<SelectionResult> getSelectionIterator(
			List<Integer> populationFitness);
}
