package me.arkorwan.cs613.ga.operator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import me.arkorwan.utils.OrderedPair;

// Elitism. Select n fittest chromosomes first (further operation not allowed),
// then select the rest by using another given selection strategy
public class ElitismSelectionStrategy extends SelectionStrategy {

	private class SelectionIterator implements Iterator<SelectionResult> {

		private List<OrderedPair<Integer, Integer>> sortedFitness;
		private int eliteUsed;
		private Iterator<SelectionResult> underlyingIterator;

		SelectionIterator(List<Integer> populationFitness) {
			sortedFitness = new ArrayList<OrderedPair<Integer, Integer>>();
			for (int i = 0; i < populationFitness.size(); i++) {
				sortedFitness.add(new OrderedPair<Integer, Integer>(
						populationFitness.get(i), i));
			}
			Collections.sort(sortedFitness);
			this.eliteUsed = 0;
			this.underlyingIterator = underlying
					.getSelectionIterator(populationFitness);
		}

		@Override
		public boolean hasNext() {
			if (eliteUsed < count) {
				return true;
			} else {
				return underlyingIterator.hasNext();
			}
		}

		@Override
		public SelectionResult next() {
			if (eliteUsed < count) {
				eliteUsed += 1;
				int index = sortedFitness.size() - eliteUsed;
				SelectionResult result = new SelectionResult(
						sortedFitness.get(index).second, false);
				return result;
			} else {
				return underlyingIterator.next();
			}
		}

	}

	private int count;
	private SelectionStrategy underlying;

	ElitismSelectionStrategy(int count, SelectionStrategy underlying) {
		this.count = count;
		this.underlying = underlying;
	}

	@Override
	public Iterator<SelectionResult> getSelectionIterator(
			List<Integer> populationFitness) {
		return new SelectionIterator(populationFitness);
	}

	@Override
	public String getStrategyDescription() {
		return String.format("%s, with elitism (%d elite members)",
				underlying.getStrategyDescription(), count);
	}

}
