package me.arkorwan.cs613.ga.operator;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

// Roulette-wheel type selection strategies. Subclasses only need to supply the weight of each fitness
public abstract class AbstractRouletteSelectionStrategy extends
		SelectionStrategy {

	protected static class SelectionIterator implements
			Iterator<SelectionResult> {

		private int[] weights;
		private int maxWeight;

		protected SelectionIterator(int[] weights) {
			this.weights = weights;
			this.maxWeight = weights[weights.length - 1];
		}

		@Override
		public boolean hasNext() {
			return true;
		}

		@Override
		public SelectionResult next() {
			int r = ThreadLocalRandom.current().nextInt(maxWeight);
			int rIndex = getWeightIndexFromValue(r, weights);
			return new SelectionResult(rIndex);
		}

	}

	@Override
	public Iterator<SelectionResult> getSelectionIterator(
			List<Integer> populationFitness) {
		int[] weights = getWeights(populationFitness);
		normalizeWeights(weights);
		return new SelectionIterator(weights);
	}

	protected abstract int[] getWeights(List<Integer> populationFitness);

	// Normalize weights into cumulative form
	static void normalizeWeights(int[] weights) {
		weights[0] -= 1;
		for (int i = 1; i < weights.length; i++) {
			weights[i] += weights[i - 1];
		}
	}

	static int getWeightIndexFromValue(int r, int[] weights) {
		int rIndex = Arrays.binarySearch(weights, r);
		if (rIndex < 0) {
			// binarySearch returns (-(insertion point) - 1) if not found.
			// in that case move to next available slot
			rIndex = -(rIndex + 1);

		}
		return rIndex;
	}

}
