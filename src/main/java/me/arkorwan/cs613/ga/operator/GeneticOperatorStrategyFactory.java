package me.arkorwan.cs613.ga.operator;

// Simple factory/parser for all operator strategies.
public class GeneticOperatorStrategyFactory {

	private static String splitter = ":";

	public static SelectionStrategy getSelectionStrategy(
			String strategyDescriptor) throws GeneticOperatorException {
		String[] descriptors = strategyDescriptor.toLowerCase().split(splitter);
		return getSelectionStrategy(descriptors);
	}

	private static SelectionStrategy getSelectionStrategy(String[] descriptors)
			throws GeneticOperatorException {

		if (descriptors.length >= 2 && descriptors[0].equals("roulettewheel")) {
			double d = Double.parseDouble(descriptors[1]);
			return new RouletteWheelSelectionStrategy(d);
		} else if (descriptors.length >= 1 && descriptors[0].equals("rank")) {
			return new RankSelectionStrategy();
		} else if (descriptors.length >= 2
				&& descriptors[0].equals("tournament")) {
			int n = Integer.parseInt(descriptors[1]);
			return new TournamentSelectionStrategy(n);
		} else if (descriptors.length >= 3 && descriptors[0].equals("elitism")) {
			int n = Integer.parseInt(descriptors[1]);
			String[] nested = new String[descriptors.length - 2];
			System.arraycopy(descriptors, 2, nested, 0, nested.length);
			SelectionStrategy s = getSelectionStrategy(nested);
			return new ElitismSelectionStrategy(n, s);
		} else {
			throw new GeneticOperatorException(
					"Cannot parse strategy description");
		}

	}

	public static CrossoverStrategy getCrossoverStrategy(String descriptor)
			throws GeneticOperatorException {
		descriptor = descriptor.toLowerCase();
		if (descriptor.equals("singlepoint")) {
			return new SinglePointCrossoverStrategy();
		} else if (descriptor.equals("uniform")) {
			return new UniformCrossoverStrategy();
		} else {
			throw new GeneticOperatorException(
					"Cannot parse strategy description");
		}
	}

	public static MutationStrategy getMutationStrategy(String strategyDescriptor)
			throws GeneticOperatorException {
		String[] descriptors = strategyDescriptor.toLowerCase().split(splitter);
		return getMutationStrategy(descriptors);
	}

	private static MutationStrategy getMutationStrategy(String[] descriptors)
			throws GeneticOperatorException {

		if (descriptors.length >= 2 && descriptors[0].equals("bitflip")) {
			double d = Double.parseDouble(descriptors[1]);
			return new BitFlipMutationStrategy(d);
		} else if (descriptors.length >= 1
				&& descriptors[0].equals("bitsreverse")) {
			return new BitsReverseMutationStrategy();
		} else {
			throw new GeneticOperatorException(
					"Cannot parse strategy description");
		}
	}
}
