package me.arkorwan.cs613.ga.operator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import me.arkorwan.utils.OrderedPair;

//Rank selection strategy. It's roulette wheel style with weight equal to 1, 2, 3, ..., N
class RankSelectionStrategy extends AbstractRouletteSelectionStrategy {

	@Override
	protected int[] getWeights(List<Integer> populationFitness) {

		List<OrderedPair<Integer, Integer>> pairList = new ArrayList<OrderedPair<Integer, Integer>>();
		for (int i = 0; i < populationFitness.size(); i++) {
			pairList.add(new OrderedPair<Integer, Integer>(populationFitness
					.get(i), i));
		}

		Collections.sort(pairList);

		int[] weights = new int[populationFitness.size()];
		for (int i = 0; i < weights.length; i++) {
			weights[pairList.get(i).second] = i + 1;
		}
		return weights;
	}

	@Override
	public String getStrategyDescription() {
		return "rank selection strategy";
	}

}
