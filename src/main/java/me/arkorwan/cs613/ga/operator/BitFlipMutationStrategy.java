package me.arkorwan.cs613.ga.operator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import me.arkorwan.cs613.ga.Chromosome;
import me.arkorwan.cs613.ga.Evolvable;
import me.arkorwan.cs613.ga.EvolvableFactory;

// Bit-flipping mutation strategy. Flip n random bits, where n is calculated from the given flip ratio
class BitFlipMutationStrategy extends MutationStrategy {

	private double flipRate;

	public BitFlipMutationStrategy(double flipRate) {
		this.flipRate = flipRate;
	}

	@Override
	public <T extends Evolvable> T execute(EvolvableFactory<T> factory, T mutant) {

		Chromosome chr = mutant.getChromosome();
		int bitsToFlip = (int) Math.ceil(chr.length() * flipRate);
		if (bitsToFlip < 0)
			bitsToFlip = 0;
		else if (bitsToFlip > chr.length())
			bitsToFlip = chr.length();
		List<Integer> indexList = new ArrayList<Integer>(chr.length());
		for (int i = 0; i < chr.length(); i++) {
			indexList.add(i);
		}
		Collections.shuffle(indexList);
		for (int i = 0; i < bitsToFlip; i++) {
			chr.flipBit(indexList.get(i));
		}
		return factory.getNewInstance(chr);
	}

	@Override
	public String getStrategyDescription() {
		return String
				.format("Bit flipping mutation strategy with expected flip bits = %.2f%%",
						flipRate);
	}

}
