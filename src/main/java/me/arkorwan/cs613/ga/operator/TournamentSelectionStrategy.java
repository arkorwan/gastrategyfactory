package me.arkorwan.cs613.ga.operator;

import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

import me.arkorwan.utils.Pair;

// Tournament selection strategy. Randomly selected N population and return the
// best one.
class TournamentSelectionStrategy extends SelectionStrategy {

	private class SelectionIterator implements Iterator<SelectionResult> {

		private List<Integer> populationFitness;

		private SelectionIterator(List<Integer> populationFitness) {
			this.populationFitness = populationFitness;
		}

		@Override
		public boolean hasNext() {
			return true;
		}

		@Override
		public SelectionResult next() {

			Pair<Integer, Integer> best = randomFitness();
			for (int i = 1; i < tournamentSize; i++) {
				Pair<Integer, Integer> f = randomFitness();
				if (f.first > best.first) {
					best = f;
				}
			}
			return new SelectionResult(best.second);
		}

		Pair<Integer, Integer> randomFitness() {
			int index = ThreadLocalRandom.current().nextInt(
					populationFitness.size());
			int fitness = populationFitness.get(index);
			return new Pair<Integer, Integer>(fitness, index);
		}

	}

	private int tournamentSize;

	TournamentSelectionStrategy(int size) {
		tournamentSize = size;
	}

	@Override
	public Iterator<SelectionResult> getSelectionIterator(
			List<Integer> populationFitness) {
		return new SelectionIterator(populationFitness);
	}

	@Override
	public String getStrategyDescription() {
		return "Tournament selection strategy with tournament size "
				+ tournamentSize;
	}

}
