package me.arkorwan.cs613.ga.operator;

public class GeneticOperatorException extends Exception{

	private static final long serialVersionUID = -8694596844749212859L;

	public GeneticOperatorException(String message){
		super(message);
	}
	
}
