package me.arkorwan.cs613.ga.operator;

import java.util.Arrays;
import java.util.List;

// Roulette wheel selection strategy. (Concrete)
public class RouletteWheelSelectionStrategy extends
		AbstractRouletteSelectionStrategy {

	private double minMaxFactor;

	// Client need to provide the ratio between the selection probability of the
	// least fit member, and the selection probability of the most fit member.
	// Because the lower bound of the probabiloty is not fixed to 0, but will be
	// calculated from this value instead.
	RouletteWheelSelectionStrategy(double minMaxFactor) {
		this.minMaxFactor = minMaxFactor;
	}

	static final int TOTAL_WEIGHT = 1000;

	@Override
	protected int[] getWeights(List<Integer> populationFitness) {
		if (populationFitness.size() == 0) {
			return new int[0];
		}

		int min = populationFitness.get(0);
		int max = min;
		for (int m : populationFitness) {
			if (m < min)
				min = m;
			else if (m > max)
				max = m;
		}

		int[] weights = new int[populationFitness.size()];

		int diff = max - min;

		// handle uniform case
		if (diff == 0) {
			System.out.println("All population are the same");
			Arrays.fill(weights, 1);
			return weights;
		}

		// (MIN-L)/(MAX-L) = F
		// MIN-L = F(MAX-L)
		// -(1-F)L = F.MAX-MIN
		// L = (MIN-F.MAX)/(1-F)

		double lowBound = (min - minMaxFactor * max) / (1 - minMaxFactor);
		double windowSize = max - lowBound;
		double coef = TOTAL_WEIGHT / windowSize;

		for (int i = 0; i < weights.length; i++) {
			weights[i] = (int) Math.ceil((populationFitness.get(i) - lowBound)
					* coef);
		}
		return weights;
	}

	@Override
	public String getStrategyDescription() {
		return "Roulette wheel selection strategy";
	}
}
