package me.arkorwan.cs613.ga.operator;

import me.arkorwan.cs613.ga.Evolvable;
import me.arkorwan.cs613.ga.EvolvableFactory;
import me.arkorwan.utils.Pair;

//Parent of all crossover strategies
public abstract class CrossoverStrategy extends GeneticOperatorStrategy{

	//return a Pair of the offsprings resulting from crossing the parents over
	public abstract <T extends Evolvable> Pair<T, T> execute(EvolvableFactory<T> factory, T parent1, T parent2);
	
}
