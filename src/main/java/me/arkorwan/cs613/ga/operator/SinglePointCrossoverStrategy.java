package me.arkorwan.cs613.ga.operator;

import java.util.concurrent.ThreadLocalRandom;

import me.arkorwan.cs613.ga.Chromosome;
import me.arkorwan.cs613.ga.Evolvable;
import me.arkorwan.cs613.ga.EvolvableFactory;
import me.arkorwan.utils.Pair;

// Single-Point crossover strategy. Split both parents at the same index,
// then append the latter part of each parent to the first part of the other.
class SinglePointCrossoverStrategy extends CrossoverStrategy {

	@Override
	public <T extends Evolvable> Pair<T, T> execute(
			EvolvableFactory<T> factory, T parent1, T parent2) {

		// (length - 1) possible crossover points
		int crossPoint = 1 + ThreadLocalRandom.current().nextInt(
				parent1.getChromosome().length() - 1);

		Pair<Chromosome, Chromosome> c1 = parent1.getChromosome().split(
				crossPoint);
		Pair<Chromosome, Chromosome> c2 = parent2.getChromosome().split(
				crossPoint);

		Chromosome cross1 = c1.first.append(c2.second);
		Chromosome cross2 = c2.first.append(c1.second);

		T offspring1 = factory.getNewInstance(cross1);
		T offspring2 = factory.getNewInstance(cross2);

		return new Pair<T, T>(offspring1, offspring2);
	}

	@Override
	public String getStrategyDescription() {
		return "single-point crossover strategy";
	}

}
