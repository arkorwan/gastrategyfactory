package me.arkorwan.cs613.ga.operator;

import me.arkorwan.cs613.ga.Evolvable;
import me.arkorwan.cs613.ga.EvolvableFactory;

// Parent of all mutation strategies
public abstract class MutationStrategy extends GeneticOperatorStrategy {

	public abstract <T extends Evolvable> T execute(
			EvolvableFactory<T> factory, T mutant);

}
