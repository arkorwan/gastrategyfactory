package me.arkorwan.cs613.ga.operator;

// Parent of all operator strategies
public abstract class GeneticOperatorStrategy {
	public abstract String getStrategyDescription();
	
	@Override
	public String toString(){
		return getStrategyDescription();
	}
}
