package me.arkorwan.cs613.ga;

// Abstract factory class for Evolvable types. Each factory has to provide
// methods for 1) create an instance based on a given Chromosome 2) create
// a random instance
public abstract class EvolvableFactory<T extends Evolvable> {

	protected abstract T createInstanceFromChromosome(Chromosome chr);

	protected abstract T createRandomInstance();

	// new Instance from Chromosome
	public T getNewInstance(Chromosome chr) {
		return createInstanceFromChromosome(chr);
	}

	// new random Instance
	public T getNewInstance() {
		return createRandomInstance();
	}
}
