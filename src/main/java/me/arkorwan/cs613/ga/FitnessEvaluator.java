package me.arkorwan.cs613.ga;

// FitnessEvaluator<T> evaluates fitness score for Evolvable type T.
// Rules:
// 1. Fitness is an int.
// 2. If A is a solution closer to soving the problem than B,
//    then fitness(A) >= fitness(B)
public interface FitnessEvaluator<T extends Evolvable> {

	int getFitness(T object);

	// This should return the fitness of the perfect solution. GA loop
	// will terminate once it finds solution with fitness equal to this
	// value. If best fitness is not know, use Integer.MAX_VALUE.
	int bestFitness();
}
